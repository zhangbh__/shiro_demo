package demo.shiro.action;

import org.smart4j.framework.mvc.annotation.Action;
import org.smart4j.framework.mvc.annotation.Request;
import org.smart4j.framework.mvc.bean.View;

@Action
public class SpaceAction {

    @Request.Get("/space")
    public View index() {
        return new View("space.jsp");
    }
}
